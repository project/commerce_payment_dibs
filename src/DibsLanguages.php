<?php

namespace Drupal\commerce_payment_dibs;

/**
 * Class DibsLanguages
 *
 * @package Drupal\commerce_payment_dibs
 */
class DibsLanguages {

  const languages = [
    'da',
    'en',
    'de',
    'es',
    'fi',
    'fo',
    'fr',
    'it',
    'nl',
    'no',
    'pl',
    'sv',
    'kl',
  ];

}