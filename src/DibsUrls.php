<?php

namespace Drupal\commerce_payment_dibs;

/**
 * Class DibsUrls
 *
 * @package Drupal\commerce_payment_dibs
 */
class DibsUrls {

  const DIBS_REDIRECT_URL = 'https://payment.architrade.com/paymentweb/start.action';

}